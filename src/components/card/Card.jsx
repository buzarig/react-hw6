import { useState, useEffect } from "react";
import styles from "./Card.module.scss";
import Button from "../button/Button";
import { Icon } from "@iconify/react";
import PropTypes from "prop-types";

const Card = ({
  productObj,
  imgSrc,
  title,
  color,
  price,
  updateCart,
  updateFavorite,
  openModal,
  favoriteRemover,
  cartRemover,
  favoriteProducts,
  cartProducts,
}) => {
  const [icon, setIcon] = useState("bi:star");
  const [inCart, setInCart] = useState(false);
  const [inFavorite, setInFavorite] = useState(false);

  useEffect(() => {
    const favoriteArray = JSON.parse(localStorage.getItem("Favorite"));
    const favoriteProduct = favoriteArray.find(
      (product) => product.article === productObj.article
    );
    if (favoriteProduct) {
      setInFavorite(true);
      setIcon("bi:star-fill");
    }

    const cartArray = JSON.parse(localStorage.getItem("Cart"));
    const cartProduct = cartArray.find(
      (product) => product.article === productObj.article
    );
    if (cartProduct) {
      setInCart(true);
    }
  }, []);

  const addToCart = () => {
    const cartProduct = cartProducts.find(
      (product) => product.article === productObj.article
    );
    if (cartProduct) {
      return false;
    }
    localStorage.setItem("Cart", JSON.stringify([...cartProducts, productObj]));
    setInCart(true);
    updateCart();
  };

  const removeFromCart = () => {
    console.log(cartProducts);
    const updatedCartArray = cartProducts.filter(
      (product) => product.article !== productObj.article
    );
    localStorage.setItem("Cart", JSON.stringify(updatedCartArray));
    setInCart(false);
    updateCart();
  };

  const addToFavorite = () => {
    const favoriteProduct = favoriteProducts.find(
      (product) => product.article === productObj.article
    );
    if (favoriteProduct) {
      return false;
    }
    localStorage.setItem(
      "Favorite",
      JSON.stringify([...favoriteProducts, productObj])
    );
    setInFavorite(true);
    updateFavorite();
    setIcon("bi:star-fill");
  };

  const removeFromFavorite = () => {
    const updatedFavoriteArray = favoriteProducts.filter(
      (product) => product.article !== productObj.article
    );
    localStorage.setItem("Favorite", JSON.stringify(updatedFavoriteArray));
    setInFavorite(false);
    updateFavorite();
    setIcon("bi:star");
  };

  const toggleFavorite = () => {
    inFavorite ? removeFromFavorite() : addToFavorite();
  };

  return (
    <div className={styles.Card}>
      <div>
        <img className={styles.CardImage} src={imgSrc} alt={title} />
        {cartRemover ? (
          <Button
            text="X"
            background="red"
            onClick={() => {
              openModal("removeFromCartModal", removeFromCart);
            }}
          />
        ) : null}
      </div>
      <div className={styles.CardContent}>
        <h1 className={styles.CardTitle}>{title}</h1>
        <Icon
          onClick={favoriteRemover ? toggleFavorite : addToFavorite}
          icon={icon}
        />
        <p>{color}</p>
        <span className={styles.CardPrice}>${price}</span>
        <Button
          onClick={
            inCart
              ? null
              : () => {
                  openModal("toCartModal", addToCart);
                }
          }
          text={inCart ? "IN CART." : "ADD TO CARD"}
          background="black"
        />
      </div>
    </div>
  );
};

Card.propTypes = {
  product: PropTypes.object,
  imgSrc: PropTypes.string,
  title: PropTypes.string,
  color: PropTypes.string,
  price: PropTypes.number,
  updateCart: PropTypes.func,
  updateFavorite: PropTypes.func,
  openModal: PropTypes.func,
};

export default Card;
