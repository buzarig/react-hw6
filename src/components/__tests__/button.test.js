import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Button from "../button/Button";

describe("Button", () => {
  it("renders with default props", () => {
    const { getByText } = render(<Button text="Click" />);
    expect(getByText("Click")).toBeInTheDocument();
    expect(getByText("Click")).toHaveStyle("background: wheat");
  });

  it("renders with custom background", () => {
    const { getByText } = render(<Button text="Click" background="blue" />);
    expect(getByText("Click")).toHaveStyle("background: blue");
  });

  it("calls onClick function when clicked", () => {
    const onClickMock = jest.fn();
    const { getByText } = render(<Button text="Click" onClick={onClickMock} />);
    fireEvent.click(getByText("Click"));
    expect(onClickMock).toHaveBeenCalled();
  });

  it("renders with modal id", () => {
    const { getByText } = render(<Button text="Click" modalId="myModal" />);
    expect(getByText("Click")).toHaveAttribute("id", "myModal");
  });
});
