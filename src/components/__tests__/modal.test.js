import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Modal from "../modal/Modal";
import modalWindowDeclarations from "../modal/modalSettings";

describe("Modal component", () => {
  const cartModalDeclaretion = modalWindowDeclarations[0]; // можемо змінювати індекс та перевіряти інші айді
  const closeModal = jest.fn();
  const submitModal = jest.fn();
  const defaultProps = {
    modalId: cartModalDeclaretion.modalId,
    close: closeModal,
    submit: submitModal,
  };

  beforeEach(() => {
    render(<Modal {...defaultProps} />);
  });

  it("should render modal with correct title and description", () => {
    const titleElement = screen.getByText(
      cartModalDeclaretion.modalProps.title
    );
    const descriptionElement = screen.getByText(
      cartModalDeclaretion.modalProps.description
    );

    expect(titleElement).toBeInTheDocument();
    expect(descriptionElement).toBeInTheDocument();
  });

  it("should call close function when close button is clicked", () => {
    const closeButton = screen.getByText("x");

    fireEvent.click(closeButton);

    expect(closeModal).toHaveBeenCalledTimes(1);
  });

  it("should call close and submit functions when submit button is clicked", () => {
    const submitButton = screen.getByText(
      cartModalDeclaretion.modalProps.actions[1].text
    );

    fireEvent.click(submitButton);

    expect(submitModal).toHaveBeenCalledTimes(1);
    expect(closeModal).toHaveBeenCalledTimes(1);
  });
});
