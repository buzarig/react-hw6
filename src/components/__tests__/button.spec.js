import React from "react";
import renderer from "react-test-renderer";
import Button from "../button/Button";

describe("Button component", () => {
  test("Matches the snapshot", () => {
    const tree = renderer.create(<Button text="Click me" />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
