import React from "react";
import renderer from "react-test-renderer";
import Products from "../products/Products";

describe("Products component", () => {
  test("Matches the snapshot", () => {
    const products = [
      {
        name: "Футболка чоловіча",
        price: 250,
        image:
          "https://randomwordgenerator.com/img/picture-generator/57e5d543495aac14f1dc8460962e33791c3ad6e04e50744172287ad19544c5_640.jpg",
        article: "123",
        color: "чорний",
      },
      {
        name: "Футболка жіноча",
        price: 220,
        image:
          "https://randomwordgenerator.com/img/picture-generator/53e4d0444d5aad14f1dc8460962e33791c3ad6e04e507440722d72d5954ec2_640.jpg",
        article: "789112",
        color: "білий",
      },
    ];

    const component = renderer.create(<Products products={products} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
