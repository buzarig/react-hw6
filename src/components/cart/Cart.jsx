import React from "react";
import { Icon } from "@iconify/react";
import styles from "./Cart.module.scss";

const Cart = ({ counter }) => {
  return (
    <div className={styles.Container}>
      <span className={styles.Count}>{counter}</span>
      <Icon
        icon="material-symbols:shopping-cart-outline-rounded"
        width="50"
        height="50"
        color="white"
      />
    </div>
  );
};

export default Cart;
