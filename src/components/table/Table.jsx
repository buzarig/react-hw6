import React from "react";
import styles from "./Table.module.scss";

function TableProducts({ products }) {
  return (
    <div>
      <table>
        <thead>
          <tr>
            <th>Article</th>
            <th>Image</th>
            <th>Name</th>
            <th>Color</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => (
            <tr key={product.article}>
              <td className={styles.Column}>{product.article}</td>
              <td>
                <img
                  className={styles.Img}
                  src={product.image}
                  alt={product.name}
                />
              </td>
              <td className={styles.Column}>{product.name}</td>
              <td className={styles.Column}>{product.color}</td>
              <td className={styles.Column}>{product.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default TableProducts;
