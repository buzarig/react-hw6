import "./App.css";
import { NavLink, Route, Routes } from "react-router-dom";
import { Home, CartPage, FavoritePage } from "./pages";
import { ViewProvider } from "./context/view";

function App() {
  return (
    <>
      <nav className="nav">
        <NavLink to="/">Home</NavLink>
        <NavLink to="/cart">Cart</NavLink>
        <NavLink to="/favorite">Favorite</NavLink>
      </nav>
      <Routes>
        <Route path="/favorite" element={<FavoritePage />} />
        <Route path="/cart" element={<CartPage />} />
        <Route
          path="/"
          element={
            <ViewProvider>
              <Home />
            </ViewProvider>
          }
        />
      </Routes>
    </>
  );
}

export default App;
