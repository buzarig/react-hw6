import React, { createContext, useState, useContext } from "react";

const ViewContext = createContext("cards");

export const useView = () => useContext(ViewContext);

export function ViewProvider({ children }) {
  const [view, setView] = useState("cards");
  return (
    <ViewContext.Provider value={view}>
      <ViewContextSet.Provider value={setView}>
        {children}
      </ViewContextSet.Provider>
    </ViewContext.Provider>
  );
}

export const ViewContextSet = createContext(null);
