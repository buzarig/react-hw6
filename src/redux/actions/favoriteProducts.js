import { productsTypes } from "../types";

export const setFavoriteProducts = (favortireProducts) => ({
  type: productsTypes.SET_FAVORITE_PRODUCTS,
  payload: favortireProducts,
});
