import { modalTypes } from "../types";

export function openModal(modalId, modalSubmit) {
  return {
    type: modalTypes.MODAL_OPEN,
    payload: { modalId, modalSubmit },
  };
}

export function closeModal() {
  return {
    type: modalTypes.MODAL_CLOSE,
  };
}
