import { combineReducers } from "redux";

import { modalReducer as modal } from "./reducers/modal";
import { productsReducer as products } from "./reducers/products";
import { favoriteProductsReducer as favorite } from "./reducers/favoriteProducts";
import { cartProductsReducer as cart } from "./reducers/cartProducts";

export const rootReducer = combineReducers({
  modal,
  products,
  favorite,
  cart,
});
