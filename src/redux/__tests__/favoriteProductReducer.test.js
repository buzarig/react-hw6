import {
  favoriteProductsReducer,
  initialState,
} from "../reducers/favoriteProducts";
import { productsTypes } from "../types";

describe("favoriteProductsReducer", () => {
  it("should return initial state", () => {
    expect(favoriteProductsReducer(undefined, {})).toEqual(initialState);
  });

  it("test handle SET_FAVORITE_PRODUCTS", () => {
    const testProducts = [{ id: 1, name: "Product 1" }];
    const action = {
      type: productsTypes.SET_FAVORITE_PRODUCTS,
      payload: testProducts,
    };

    expect(favoriteProductsReducer(undefined, action)).toEqual({
      favoriteProducts: testProducts,
    });
  });
});
