import { modalReducer, initialState } from "../reducers/modal";
import { modalTypes } from "../types";

describe("modalReducer", () => {
  it("should return initial state", () => {
    expect(modalReducer(undefined, {})).toEqual(initialState);
  });

  it("test handle MODAL_OPEN", () => {
    const testPayload = { modalId: "test-modal", modalSubmit: () => {} };
    const action = { type: modalTypes.MODAL_OPEN, payload: testPayload };

    expect(modalReducer(undefined, action)).toEqual({
      showModal: true,
      modalId: "test-modal",
      modalSubmit: testPayload.modalSubmit,
    });
  });

  it("test handle MODAL_CLOSE", () => {
    const action = { type: modalTypes.MODAL_CLOSE };

    expect(modalReducer(undefined, action)).toEqual({
      showModal: false,
      modalId: null,
      modalSubmit: null,
    });
  });
});
