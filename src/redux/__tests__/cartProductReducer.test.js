import { cartProductsReducer, initialState } from "../reducers/cartProducts";
import { productsTypes } from "../types";

describe("cartProductsReducer", () => {
  it("should return the initial state", () => {
    expect(cartProductsReducer(undefined, {})).toEqual(initialState);
  });

  it("test handle SET_CART_PRODUCTS", () => {
    const products = [
      {
        id: 1,
        name: "Product 1",
        price: 10,
      },
      {
        id: 2,
        name: "Product 2",
        price: 20,
      },
    ];

    expect(
      cartProductsReducer(undefined, {
        type: productsTypes.SET_CART_PRODUCTS,
        payload: products,
      })
    ).toEqual({
      cartProducts: products,
    });
  });
});
