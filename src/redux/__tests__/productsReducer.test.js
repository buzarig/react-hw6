import { productsReducer, initialState } from "../reducers/products";
import { productsTypes } from "../types";

describe("productsReducer", () => {
  it("should return initial state", () => {
    expect(productsReducer(undefined, {})).toEqual(initialState);
  });

  it("test handle FETCH_PRODUCTS_SUCCESS", () => {
    const mockProducts = [{ id: 1, name: "Product 1" }];
    const action = {
      type: productsTypes.FETCH_PRODUCTS_SUCCESS,
      payload: mockProducts,
    };

    expect(productsReducer(undefined, action)).toEqual({
      currentProducts: mockProducts,
    });
  });
});
