import React, { useState, useEffect } from "react";
import Products from "../components/products/Products";
import Modal from "../components/modal/Modal";
import styles from "../components/form-cart/Form-cart.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { openModal, closeModal } from "../redux/actions/modal";
import { setCartProducts } from "../redux/actions/cartProducts";
import { setFavoriteProducts } from "../redux/actions/favoriteProducts";
import FormCart from "../components/form-cart/Form-cart";

export function CartPage() {
  const dispatch = useDispatch();
  const modal = useSelector((state) => state.modal);

  const [products, setProducts] = useState([]);
  const favoriteProducts = useSelector(
    (state) => state.favorite.favoriteProducts
  );
  const cartProducts = useSelector((state) => state.cart.cartProducts);

  useEffect(() => {
    const productsInCart = JSON.parse(localStorage.getItem("Cart"));
    setProducts(productsInCart);
  }, [cartProducts]);

  if (cartProducts.length === 0) {
    return <p>no products in cart.</p>;
  }

  const updateCart = () => {
    const updateCartArray = JSON.parse(localStorage.getItem("Cart"));
    dispatch(setCartProducts(updateCartArray));
  };

  const updateFavorite = () => {
    const updateFavoriteArray = JSON.parse(localStorage.getItem("Favorite"));
    dispatch(setFavoriteProducts(updateFavoriteArray));
  };

  const handleOpenModal = (modalId, modalSubmit) => {
    dispatch(openModal(modalId, modalSubmit));
  };

  const handleCloseModal = () => {
    dispatch(closeModal());
  };

  return (
    <>
      <div className={styles.Cart}>
        <Products
          products={products}
          openModal={handleOpenModal}
          updateCart={updateCart}
          updateFavorite={updateFavorite}
          favoriteProducts={favoriteProducts}
          cartProducts={cartProducts}
          cartRemover={true}
        />
        <FormCart updateCart={updateCart} />
        {modal.showModal && (
          <Modal
            close={handleCloseModal}
            modalId={modal.modalId}
            submit={modal.modalSubmit}
          />
        )}
      </div>
    </>
  );
}
