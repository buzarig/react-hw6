import React, { useState, useEffect } from "react";
import Products from "../components/products/Products";
import Modal from "../components/modal/Modal";
import { useDispatch, useSelector } from "react-redux";
import { openModal, closeModal } from "../redux/actions/modal";
import { setCartProducts } from "../redux/actions/cartProducts";
import { setFavoriteProducts } from "../redux/actions/favoriteProducts";

export function FavoritePage() {
  const dispatch = useDispatch();
  const modal = useSelector((state) => state.modal);

  const [products, setProducts] = useState([]);
  const favoriteProducts = useSelector(
    (state) => state.favorite.favoriteProducts
  );
  const cartProducts = useSelector((state) => state.cart.cartProducts);

  useEffect(() => {
    const productsInFavorite = JSON.parse(localStorage.getItem("Favorite"));
    setProducts(productsInFavorite);
  }, [favoriteProducts]);

  if (favoriteProducts.length === 0) {
    return <p>no products in favorite.</p>;
  }

  const updateCart = () => {
    const updateCartArray = JSON.parse(localStorage.getItem("Cart"));
    dispatch(setCartProducts(updateCartArray));
  };

  const updateFavorite = () => {
    const updateFavoriteArray = JSON.parse(localStorage.getItem("Favorite"));
    dispatch(setFavoriteProducts(updateFavoriteArray));
  };

  const handleOpenModal = (modalId, modalSubmit) => {
    dispatch(openModal(modalId, modalSubmit));
  };

  const handleCloseModal = () => {
    dispatch(closeModal());
  };

  return (
    <>
      <Products
        favoriteProducts={favoriteProducts}
        cartProducts={cartProducts}
        products={products}
        updateFavorite={updateFavorite}
        updateCart={updateCart}
        openModal={handleOpenModal}
        favoriteRemover={true}
      />
      {modal.showModal && (
        <Modal
          close={handleCloseModal}
          modalId={modal.modalId}
          submit={modal.modalSubmit}
        />
      )}
    </>
  );
}
