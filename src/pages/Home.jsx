import Products from "../components/products/Products";
import React, { useEffect, useContext } from "react";
import Modal from "../components/modal/Modal";
import Cart from "../components/cart/Cart";
import Favorite from "../components/favorite/Favorite";
import { useDispatch, useSelector } from "react-redux";
import { openModal, closeModal } from "../redux/actions/modal";
import { fetchProducts } from "../redux/actions/products";
import { setCartProducts } from "../redux/actions/cartProducts";
import { setFavoriteProducts } from "../redux/actions/favoriteProducts";

import { useView, ViewContextSet } from "../context/view";
import TableProducts from "../components/table/Table";

export function Home() {
  const dispatch = useDispatch();
  const modal = useSelector((state) => state.modal);
  const products = useSelector((state) => state.products.currentProducts);
  const favoriteProducts = useSelector(
    (state) => state.favorite.favoriteProducts
  );
  const cartProducts = useSelector((state) => state.cart.cartProducts);

  const view = useView();
  const setView = useContext(ViewContextSet);

  const handleViewChange = (e) => {
    setView(e.target.value);
  };

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  const updateCart = () => {
    const updateCartArray = JSON.parse(localStorage.getItem("Cart"));
    dispatch(setCartProducts(updateCartArray));
  };

  const updateFavorite = () => {
    const updateFavoriteArray = JSON.parse(localStorage.getItem("Favorite"));
    dispatch(setFavoriteProducts(updateFavoriteArray));
  };

  const handleOpenModal = (modalId, modalSubmit) => {
    dispatch(openModal(modalId, modalSubmit));
  };

  const handleCloseModal = () => {
    dispatch(closeModal());
  };

  return (
    <div>
      <header className="header">
        <Favorite counter={favoriteProducts.length} />
        <Cart counter={cartProducts.length} />
        <div>
          <label style={{ color: "white" }}>
            <input
              type="radio"
              value="cards"
              checked={view === "cards"}
              onChange={handleViewChange}
            />
            Cards
          </label>
          <label style={{ color: "white" }}>
            <input
              type="radio"
              value="table"
              checked={view === "table"}
              onChange={handleViewChange}
            />
            Table
          </label>
        </div>
      </header>
      {view === "cards" ? (
        <Products
          products={products}
          favoriteProducts={favoriteProducts}
          cartProducts={cartProducts}
          openModal={handleOpenModal}
          updateFavorite={updateFavorite}
          updateCart={updateCart}
        />
      ) : (
        <TableProducts products={products} />
      )}
      {modal.showModal && (
        <Modal
          close={handleCloseModal}
          modalId={modal.modalId}
          submit={modal.modalSubmit}
        />
      )}
    </div>
  );
}
